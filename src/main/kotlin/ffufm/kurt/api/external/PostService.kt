package ffufm.kurt.api.external

import ffufm.kurt.api.spec.dbo.password.PasswordPassword
import ffufm.kurt.api.spec.dbo.post.PostPost
import ffufm.kurt.api.spec.dbo.url.UrlUrl
import org.springframework.core.env.Environment
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.awaitBody

@Service
class PostService(
    env: Environment
) {

    val token = env.getProperty("token")
    //Establish connection to the external API
    fun postApiClient() : WebClient{
        return WebClient.builder()
            .baseUrl("https://goweather.herokuapp.com/weather/Manila")
            .filter { request, next -> next.exchange(request)  }
            .build()
    }

    //Get post from external API
    suspend fun getPosts(userId: Long) : List<Any>{
        val client  = postApiClient()
        return client.get().uri{ builder -> builder
            .apply { path("/greet/") }
            .build()
        }
            .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
            .retrieve()
            .awaitBody()
    }
    suspend fun getPosts (): List<Any> {
        val client = postApiClient()
        return client.get()
            .uri{builder -> builder
                .apply { path("/posts") }
                .build()
            }
            .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
            .retrieve()
            .awaitBody()
    }

    //Create post to the external Api
    suspend fun postPosts (post: PostPost): PostPost {
        val client = postApiClient()
        return client.post()
            .uri{builder -> builder
                .apply { path("/posts") }
                .build()
            }
            .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
            .header(HttpHeaders.AUTHORIZATION,"Bearer")
            .bodyValue(post)
            .retrieve()
            .awaitBody()
    }
    suspend fun postUrl (post: UrlUrl): Map<Any, Any> {
        val client = postApiClient()
        return client.post()
            .uri{builder -> builder
                .apply { path("/api/v1/shorten") }
                .build()
            }
            .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
            .bodyValue(post)
            .retrieve()
            .awaitBody()
    }

    suspend fun generatePassword (): PasswordPassword {
        val client = postApiClient()
        return client.get()
            .uri{builder -> builder
                .apply { path("/api/Text/Password") }
                .build()
            }
            .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
            .header(HttpHeaders.AUTHORIZATION, "Bearer $token")
            .retrieve()
            .awaitBody()
    }



    //Establish connection to the external API
//    fun postApiClient() : WebClient{
//        return WebClient.builder()
//            .baseUrl("https://jsonplaceholder.typicode.com")
//            .filter { request, next -> next.exchange(request)  }
//            .build()
//    }
//
//    //Get post from external API
//    suspend fun getPosts(userId: Long) : List<Any>{
//        val client  = postApiClient()
//        return client.get().uri{ builder -> builder
//            .apply { path("/posts/") }
//            .build()
//        }
//            .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
//            .retrieve()
//            .awaitBody()
//    }

}