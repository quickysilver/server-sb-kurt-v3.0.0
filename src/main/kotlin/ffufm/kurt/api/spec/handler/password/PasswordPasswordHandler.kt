package ffufm.kurt.api.spec.handler.password

import com.fasterxml.jackson.module.kotlin.readValue
import de.ffuf.pass.common.handlers.PassMvcHandler
import ffufm.kurt.api.spec.dbo.password.PasswordPassword
import kotlin.Boolean
import kotlin.Int
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.server.ResponseStatusException

interface PasswordPasswordDatabaseHandler {
    /**
     * : 
     */
    suspend fun generatePassword(
        hasDigits: Boolean,
        hasSpecial: Int,
        hasUppercase: Boolean,
        length: Int
    ): PasswordPassword
}

@Controller("password.Password")
class PasswordPasswordHandler : PassMvcHandler() {
    @Autowired
    lateinit var databaseHandler: PasswordPasswordDatabaseHandler

    /**
     * : 
     */
    @RequestMapping(value = ["/api/Text/Password"], method = [RequestMethod.GET])
    suspend fun generatePassword(
        @RequestParam("hasDigits") hasDigits: Boolean,
        @RequestParam("hasSpecial") hasSpecial: Int,
        @RequestParam("hasUppercase") hasUppercase: Boolean,
        @RequestParam("length") length: Int
    ): ResponseEntity<*> {

        return success { databaseHandler.generatePassword(hasDigits, hasSpecial,
                hasUppercase, length) }
    }
}
