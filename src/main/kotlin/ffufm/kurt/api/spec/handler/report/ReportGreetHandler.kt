package ffufm.kurt.api.spec.handler.report

import com.fasterxml.jackson.module.kotlin.readValue
import de.ffuf.pass.common.handlers.PassMvcHandler
import ffufm.kurt.api.spec.dbo.report.ReportGreet
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.server.ResponseStatusException

interface ReportGreetDatabaseHandler {
    /**
     * : 
     * HTTP Code 200: Return greetings from API
     */
    suspend fun greetUser(): ReportGreet?
}

@Controller("report.Greet")
class ReportGreetHandler : PassMvcHandler() {
    @Autowired
    lateinit var databaseHandler: ReportGreetDatabaseHandler

    /**
     * : 
     * HTTP Code 200: Return greetings from API
     */
    @RequestMapping(value = ["/greet/"], method = [RequestMethod.GET])
    suspend fun greetUser(): ResponseEntity<*> {

        return success { databaseHandler.greetUser() ?: throw
                ResponseStatusException(HttpStatus.NOT_FOUND) }
    }
}
