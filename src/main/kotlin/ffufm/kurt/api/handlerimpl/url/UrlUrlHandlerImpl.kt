package ffufm.kurt.api.handlerimpl.url

import ffufm.kurt.api.external.PostService
import ffufm.kurt.api.spec.dbo.report.ReportReport
import ffufm.kurt.api.spec.dbo.url.UrlUrl
import ffufm.kurt.api.spec.handler.url.UrlUrlDatabaseHandler
import org.springframework.stereotype.Component

@Component("url.UrlUrlHandler")
class UrlUrlHandlerImpl (
    val postApi: PostService
    ) : UrlUrlDatabaseHandler {

    override suspend fun create(body: UrlUrl): Map<Any,Any> {
        return postApi.postUrl(body)
    }
}
