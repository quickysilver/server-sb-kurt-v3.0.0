package ffufm.kurt.api.handlerimpl.post

import ffufm.kurt.api.external.PostService
import ffufm.kurt.api.spec.dbo.post.PostPost
import ffufm.kurt.api.spec.handler.post.PostPostDatabaseHandler
import org.springframework.stereotype.Component

@Component("post.PostPostHandler")
class PostPostHandlerImpl(
    val postApi: PostService
): PostPostDatabaseHandler {
    override suspend fun create(body: PostPost): PostPost{
        return postApi.postPosts(body)
    }
}