package ffufm.kurt.api.handlerimpl.report

import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import ffufm.kurt.api.external.PostService
import ffufm.kurt.api.spec.dbo.report.ReportReport
import ffufm.kurt.api.spec.handler.report.ReportReportDatabaseHandler
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component

@Component("report.ReportReportHandler")
class ReportReportHandlerImpl(
    val postApi: PostService
) : ReportReportDatabaseHandler {
    /**
     * : 
     * HTTP Code 200: Return total number of posts from the API
     */
    override suspend fun totalPosts(userId: Long): ReportReport? {
        val post = postApi.getPosts()
        val totalPosts = post.size
        return ReportReport(totalPosts = totalPosts)
    }
}
