package ffufm.kurt.api.handlerimpl.password

import ffufm.kurt.api.external.PostService
import ffufm.kurt.api.spec.dbo.password.PasswordPassword
import ffufm.kurt.api.spec.handler.password.PasswordPasswordDatabaseHandler
import org.springframework.data.domain.Page
import org.springframework.stereotype.Component

@Component("password.PasswordPasswordHandler")
class PasswordPasswordHandlerImpl(
    val postApi: PostService
): PasswordPasswordDatabaseHandler {

    override suspend fun generatePassword(
        hasDigits: Boolean,
        hasSpecial: Int,
        hasUppercase: Boolean,
        length: Int
    ): PasswordPassword {
        return postApi.generatePassword()
    }
}